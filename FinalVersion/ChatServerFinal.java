import java.net.*;
import java.io.*;
import java.security.*;


public class ChatServerFinal  implements Runnable
{  
    private ChatServerThreadFinal  servants[] = new ChatServerThreadFinal [50];
    private ServerSocket server = null;
    private Thread       thread = null;
    private int clientCount = 0;

    //Security Related Code
    AdaptedCryptoManager acm;
    KeyPair pair;
    PublicKey pub;
    PrivateKey priv;

    // Database handle
    DBConnect db;


    public ChatServerFinal (int port)
    {  
	try
	    {  

		acm = new AdaptedCryptoManager();
		db = new DBConnect();
		pair = acm.getKeyPair();
		pub = pair.getPublic();
		priv = pair.getPrivate();

		servants = new ChatServerThreadFinal [50];

		System.out.println("Binding to port " + port + ", please wait  ...");
		server = new ServerSocket(port);  
		System.out.println("Server started: " + server);
		start(); 
	    }
	catch(Exception e)
	    {  
		System.out.println("Exception : " + e.getMessage()); 
		e.printStackTrace();
	    }
    }



    public void run()
    {  
	while (thread != null)
	    {  
		try
		    {  
			System.out.println("Waiting for a client ..."); 
			addThread(server.accept()); 
		    }
             catch(IOException ioe)
		 {  
		     System.out.println("Server accept error: " + ioe); 
		     stop(); 
		 }
	    }
    }
    public void start()  
    { 

	if (thread == null)
	    {  
		thread = new Thread(this); 
		thread.start();
	    }
    }
    public void stop()   
    { 

	if (thread != null)
	    { 
		thread.stop(); 
		thread = null;
	    }	
    }
    private int findClient(int ID)
    {  
	for (int i = 0; i < clientCount; i++)
	    if (servants[i].getID() == ID)
                return i;
	return -1;
    }
     
    public synchronized void handle(int ID, String input)
    {  
	if (input.equals(".bye"))
	    {  
		servants[findClient(ID)].send(".bye");
		remove(ID); 
	    }
	else
	    for (int i = 0; i < clientCount; i++)
		servants[i].send(input);   
                //servants[i].send(ID + ": " + input);   
    }
 
    public synchronized void remove(int ID)
       {  
	   int pos = findClient(ID);
	   if (pos >= 0)
	      {  
		  ChatServerThreadFinal toTerminate = servants[pos];
		  System.out.println("Removing client thread " + ID + " at " + pos);
		  if (pos < clientCount-1)
		      for (int i = pos+1; i < clientCount; i++)
			  servants[i-1] = servants[i];
		  clientCount--;
		  try
		      {  
			  toTerminate.close(); 
		      }
		  catch(IOException ioe)
		      {  
			  System.out.println("Error closing thread: " + ioe); 
		      }
		  toTerminate.stop(); 
	      }
       }

    private void addThread(Socket socket)
    {  
	if (clientCount < servants.length)
	    {  
		System.out.println("Client accepted: " + socket);
		servants[clientCount] = new ChatServerThreadFinal
		    (this, socket, acm, pub, priv, db);
		try
		    {  
			servants[clientCount].open(); 
			servants[clientCount].start();
			clientCount++; 
		    }
		catch(IOException ioe)
		    {  
			System.out.println("Error opening thread: " + ioe); 
		    } 
	    }
	else
	    System.out.println("Client refused: maximum " + servants.length + " reached.");
    }
    public static void main(String args[]) 
    { /* as before */ 

	ChatServerFinal server = null;
	if (args.length != 1)
	    System.out.println("Usage: java ChatServer port");
	else
	    server = new ChatServerFinal (Integer.parseInt(args[0]));
    }
}

