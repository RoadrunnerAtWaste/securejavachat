import java.io.*;
import java.security.*;
import java.security.spec.*;
import javax.crypto.*;
import javax.crypto.spec.*;
import org.apache.commons.codec.binary.*; 
import org.apache.commons.codec.binary.Base64.*; 
 
public class AdaptedCryptoManager {

    Base64 b64;
    public AdaptedCryptoManager()
    {
        b64 = new org.apache.commons.codec.binary.Base64();
    }


    public String createHashDigest(String input)
    {
	try 
	    {
		MessageDigest md = MessageDigest.getInstance("SHA-256");
		byte[] passBytes = input.getBytes();
		md.update(passBytes);
		byte[] digest = md.digest();
		String digestString = new String(digest);
		return digestString;
	    }
	catch(Exception e) 
	    {
		e.printStackTrace();
		return null;
	    }
    }
    
    /**********************************************
     *  METHOD:  keyGenerateKey()
     *  PRECONDITIONS:  
     *  POSTCONDITIONS:  Creates a Random Secret Key
     *                       shhhhh!  don't tell anyone!
     *********************************************/    	

    public Key generateKey()  {
	try 
	    {
		KeyGenerator generator;
		generator = KeyGenerator.getInstance("DES");
		generator.init(new SecureRandom());
		Key key = generator.generateKey();
		return key;
	    }
	catch(Exception e)
	    {
		handleException (e);
		return null;
	    }
    }



    /**********************************************
     *  METHOD:  getKeyPair()
     *  PRECONDITIONS:  
     *  POSTCONDITIONS:  generates an RSA KeyPair
     *********************************************/    	


    public KeyPair getKeyPair() 
    {
	try
	    {
		/*initialize key pair*/
		KeyPairGenerator keyGen = KeyPairGenerator.getInstance("RSA");
		SecureRandom random = SecureRandom.getInstance("SHA1PRNG", "SUN");
		keyGen.initialize(1024, random);
		KeyPair pair = keyGen.generateKeyPair();
		return pair;
	    }
	catch(Exception e)
	    {
		handleException (e);
		return null;
	    }
    }



    /**********************************************
     *  METHOD:     public String encrypt(String message, Key key) 
     *  PRECONDITIONS:  
     *  POSTCONDITIONS:  encrypts a message using a key
     *********************************************/    	

 
    public String encrypt(String message, Key key) 
    {
	try
	    {
		// Get a cipher object.
		Cipher cipher = Cipher.getInstance("DES/ECB/PKCS5Padding");
		cipher.init(Cipher.ENCRYPT_MODE, key);
		
		// Gets the raw bytes to encrypt, UTF8 is needed for
		// having a standard character set
		byte[] stringBytes = message.getBytes("UTF8");
		
		// encrypt using the cypher
		byte[] raw = cipher.doFinal(stringBytes);
		
		// converts to base64 for easier display.
		//	BASE64Encoder encoder = new BASE64Encoder();
		//	String base64 = encoder.encode(raw);
		String base64 = b64.encodeBase64String(raw);
		
		return base64;
	    }
	catch(Exception e)
	    {
		handleException (e);
		return null;
	    }	
    }

    /**********************************************
     *  METHOD:    public String decrypt(String message, Key key) 
     *  PRECONDITIONS:  
     *  POSTCONDITIONS:  decrypts message using a key
     *********************************************/    	
 
    public String decrypt(String encrypted, Key key) 
    {
	try 
	    {
		// Get a cipher object.
		Cipher cipher = Cipher.getInstance("DES/ECB/PKCS5Padding");
		cipher.init(Cipher.DECRYPT_MODE, key);
 
		//decode the BASE64 coded message
		//BASE64Decoder decoder = new BASE64Decoder();
		//byte[] raw = decoder.decodeBuffer(encrypted);
		byte [] raw = b64.decodeBase64(encrypted);
		
		//decode the message
		byte[] stringBytes = cipher.doFinal(raw);
 
		//converts the decoded message to a String
		String clear = new String(stringBytes, "UTF8");
		return clear;
	    }
	catch(Exception e)
	    {
		handleException (e);
		return null;
	    }
    }


    /**********************************************
     *  METHOD: public String encryptSecretKeyWithPublicKey 
     *  PRECONDITIONS:  
     *  POSTCONDITIONS:  Returns a String that represents
     *                       the Secret Key encrypted with the Public Key
     *********************************************/    	

    public String encryptSecretKeyWithPublicKeyAsString 
	(Key sk, PublicKey pk)
    {
	try 
	    {
		byte[] secretBytes = sk.getEncoded(); 
		Cipher cipher = Cipher.getInstance("RSA");
		cipher.init(Cipher.ENCRYPT_MODE, pk);
		byte[] cipherData = cipher.doFinal(secretBytes);
		String base64 = b64.encodeBase64String(cipherData);
		return base64;
	    }

	catch (Exception e)
	    {
		handleException(e);
		return null;
	    }
    }


    /**********************************************
     *  METHOD:     public Key decryptSecretKeyWithPrivateKey 
     *  PRECONDITIONS:  
     *  POSTCONDITIONS:  returns a Secet Key
     *********************************************/    	

    public Key decryptSecretKeyAsStringWithPrivateKey 
	(String keyAsString, PrivateKey pk)
    {
	try 
	    {
		byte [] raw = b64.decodeBase64(keyAsString);
		
		//decode the message
		Cipher cipher = Cipher.getInstance("RSA");
		cipher.init(Cipher.DECRYPT_MODE, pk);
		byte[] bytes = cipher.doFinal(raw);
		
		DESKeySpec desKeySpec = new DESKeySpec(bytes);
		SecretKeyFactory keyFactory = SecretKeyFactory.getInstance("DES");
		Key key = keyFactory.generateSecret(desKeySpec);
	       return key;
	    }
	catch (Exception e)
	    {
		handleException(e);
		return null;
	    }
    }


    /****************************************************
     *
     *  CONVERSION FUNCTIONS
     *
     ****************************************************/

    /**********************************************
     *  METHOD:  conver
     *  PRECONDITIONS:  
     *  POSTCONDITIONS:  
     *********************************************/    	


    public String convertPublicKeyToString(PublicKey pk)
    {
	String stringKey = b64.encodeBase64String( pk.getEncoded());
	return stringKey;

    }

    /**********************************************
     *  METHOD:  getKeyPair()
     *  PRECONDITIONS:  
     *  POSTCONDITIONS:  generates an RSA KeyPair
     *********************************************/    	

    public PublicKey convertStringToPublicKey(String keyAsString)
    {
	try 
	    {
		X509EncodedKeySpec spec =
		    new X509EncodedKeySpec
		    (b64.decodeBase64( keyAsString));
		KeyFactory kf = KeyFactory.getInstance("RSA");
		return kf.generatePublic(spec);
	    }
	catch (Exception e)
	    {
		e.printStackTrace();
		return null;
	    }
    }



    /**********************************************
     *  METHOD:  getKeyPair()
     *  PRECONDITIONS:  
     *  POSTCONDITIONS:  generates an RSA KeyPair
     *********************************************/    	

    






    /**********************************************
     *  METHOD:  getKeyPair()
     *  PRECONDITIONS:  
     *  POSTCONDITIONS:  generates an RSA KeyPair
     *********************************************/    	


    public static void main(String args[])
    {
       
	try 
	    {
		AdaptedCryptoManager acm = new AdaptedCryptoManager();
		Key sk = acm.generateKey();
		KeyPair kp = acm.getKeyPair();
		PublicKey pub =  kp.getPublic();
		PrivateKey priv = kp.getPrivate();
		String testString = "hello cryptography";
		
		System.out.println("\n\nFirst Encryption Test:");
		acm.testEncryptDecrypt(testString, sk);
		

		System.out.println("\n\nSecond Encryption Test:");
		acm.testPK2StrConversion(pub);

		System.out.println("\n\nThird Encryption Test:");
		acm.testEncryptionDecryptionOfSecretByPublic
		    (pub, priv, sk);

	    }
	catch (Exception e)
	    {
		handleException(e);
	    }	
    }

    public boolean testEncryptDecrypt(String testString, Key key)
    {
	String plaintext = testString;
	System.out.println("plaintext is " + plaintext);
	
	String encrypted = encrypt(plaintext, key);	
	System.out.println("Encrypted is " + encrypted);
	
	String decrypted = decrypt(encrypted, key);
	System.out.println("Decrypted is " + decrypted);

	if (!(plaintext.equals(decrypted)))
	    {
		System.err.println(" Encryption/Decryption Failure");
		assert(false);
		return false;
	    }
	return true;
    }

    public boolean testEncryptionDecryptionOfSecretByPublic
	(PublicKey pub,  PrivateKey priv, Key sk)
    {
	System.out.println("Secret Key is  " + sk);
	String tempSK =     encryptSecretKeyWithPublicKeyAsString 
	    (sk, pub);

	Key decryptedKey = decryptSecretKeyAsStringWithPrivateKey 
	    (tempSK, priv);
	System.out.println("Secret Key is  " + decryptedKey);

	return false;
    }

    public boolean testPK2StrConversion(PublicKey pub)
    {
	System.out.println("Public key is " + pub);
	String tempPub = convertPublicKeyToString( pub);
	System.out.println("Converted to string is " + tempPub);
	PublicKey anotherPK = convertStringToPublicKey(tempPub);
	System.out.println("another pk is  " + anotherPK);
	return true;
    }
    public static void handleException(Exception e)
    {
	System.out.println("EXCEPTION:  " + e.getMessage());
	e.printStackTrace();
    }
}