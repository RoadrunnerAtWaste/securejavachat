import java.net.*;
import java.io.*;
import java.security.*;
import java.util.Arrays;

    public class ChatServerThreadFinal extends Thread
    {  private ChatServerFinal       server    = null;
       private Socket           socket    = null;
       private int              ID        = -1;
       private DataInputStream  streamIn  =  null;
       private DataOutputStream streamOut = null;

	private AdaptedCryptoManager acm;

	// shared with client
	private Key secretKey;
	
	// taken from server
	private KeyPair pair;   
	private PublicKey pub;  
	private PrivateKey priv;

	DBConnect db;
	

	boolean publicKeySent= false;
	boolean secretKeyReceived = false;
	boolean loginReceived = false;


       public ChatServerThreadFinal 
	   (ChatServerFinal _server,  Socket _socket,  
	    AdaptedCryptoManager acm,
	    PublicKey pub, PrivateKey priv,
	    DBConnect db) 
	{  
	    super();
	    server = _server;
	    socket = _socket;
	    ID     = socket.getPort();

	    this.acm=acm;
	    this.pub = pub;
	    this.priv = priv;

	    this.db = db;
	}
	
	public void send(String msg)
	{   
	    try
		{  
		    String encrypted = acm.encrypt(msg, secretKey);
		    streamOut.writeUTF(encrypted);
		    streamOut.flush();
		}
	    catch(Exception e)
		{  
		    System.out.println(ID + 
				       " Exception: " + e.getMessage());
		    //e.printStackTrace();
		    server.remove(ID);
		    stop();
		}
	}
	public int getID()
	{  
	    return ID;
	}
	public boolean processLogin()
	{
	    loginReceived =true;
	    try
		{
		    String cipherText = streamIn.readUTF();
		    String decrypted = acm.decrypt
			(cipherText, secretKey);

		    String [] mungeBuffer = decrypted.split("9@55");
		    String pass = mungeBuffer[1];
		    decrypted = mungeBuffer[0];
		    mungeBuffer = decrypted.split("6N0m3");
		    String name = mungeBuffer[1];
		    String dbPass = db.retrievePassword(name);
		    if (dbPass == null)
			{
			    return false;
			}
       
		    boolean match = true;
		    for (int i = 0; i < 8; i++)
			{
			    char p_char = pass.charAt(i);       
			    char db_char = dbPass.charAt(i);        
			    if (p_char != db_char) 
				{
				    match=false;
				    break;
				}
			}
		    return match;
		}
	    catch (Exception e) 
		{
		    //		    e.printStackTrace();
		    return false;
		}
	}



	public void receiveSecretKey()
	{
	    try
		{
		    String cipherText = streamIn.readUTF();
		    secretKey = 
			acm.decryptSecretKeyAsStringWithPrivateKey
			(cipherText, priv);
		    secretKeyReceived = true;
		}
	    catch (Exception e) {e.printStackTrace();}
	}
	


	public void sendPublicKey()
	{
	    try 
		{
		    
		    String pkAsString = acm.convertPublicKeyToString(pub);
		    streamOut.writeUTF(pkAsString);
		    streamOut.flush();
		    publicKeySent=true;
		}
	    catch (Exception e)
		{
		    System.out.println
			(ID + " Exception:  " + e.getMessage());
		    e.printStackTrace();
		    server.remove(ID);
		    stop();
		}
	}

	public void run()
	{  
	    System.out.println("Server Thread " + ID + " running.");
	    while (true)
		{  
		    if (!publicKeySent)
			{
			    sendPublicKey();
			}
		    else if (!secretKeyReceived)
			{
			    receiveSecretKey();
			}
		    else if (!loginReceived)
			{
			    if (!processLogin())
				{
				    send ("Stop trying to hack me, VILLAMOR!");
				    server.remove(ID);
				    stop();
				}
			}
		    else {
			try
			    {  

				String cipherText = streamIn.readUTF();
				String decrypted = acm.decrypt
				    (cipherText, secretKey);
				server.handle(ID, decrypted);
			    }
			catch(Exception e)
			    {  
				System.out.println
				    (ID + " Exception:  " + e.getMessage());
				//e.printStackTrace();
				server.remove(ID);
				stop();
			    }
		    }
		}
	}
       
	public void open() throws IOException
	{  
	    streamIn = new DataInputStream(new 
					   BufferedInputStream
					   (socket.getInputStream()));
	    streamOut = new DataOutputStream(new
                            BufferedOutputStream
					     (socket.getOutputStream()));
	}
       
	public void close() throws IOException
	{  
	    if (socket != null)    socket.close();
	    if (streamIn != null)  streamIn.close();
	    if (streamOut != null) streamOut.close();
	}
    }

