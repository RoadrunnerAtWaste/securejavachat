    import java.net.*;
    import java.io.*;
import java.security.Key;
import java.security.PublicKey;

    public class ChatClientFinal implements Runnable
    {  private Socket socket              = null;
       private Thread thread              = null;
       private DataInputStream  console   = null;
       private DataOutputStream streamOut = null;
       private ChatClientThreadFinal client    = null;


	private String username;
	private String password;


	private AdaptedCryptoManager acm;
	private Key secretKey;
	private PublicKey serverPublicKey;

	private boolean receivedPublicKey = false;
	private boolean secretKeySent = false;
	private boolean loginSent = false;

	public ChatClientFinal(String serverName, int serverPort, 
			     String username, String password)
       {  
	   System.out.println("Establishing connection. Please wait ...");
	   try
	       {  
		   this.username=username;
		   this.password = password;
		   acm = new AdaptedCryptoManager();
		   secretKey = acm.generateKey();

		   socket = new Socket(serverName, serverPort);
		   System.out.println("Connected: " + socket);
		   start();
	       }

	   catch (Exception e)
	       {
		   System.out.println("Exception:  " +  e.getMessage());
		   e.printStackTrace();
	       }
       }

    /**********************************************
     *  METHOD:  
     *  PRECONDITIONS:  
     *  POSTCONDITIONS:  
     *********************************************/    
       public void start() throws IOException
	{  
	    console   = new DataInputStream(System.in);
	    streamOut = new DataOutputStream(socket.getOutputStream());
	    if (thread == null)
		{  
		    client = new ChatClientThreadFinal (this, socket);
		    thread = new Thread(this);                   
		    thread.start();
		}
       }

    /**********************************************
     *  METHOD:  
     *  PRECONDITIONS:  
     *  POSTCONDITIONS:  
     *********************************************/    	
       public void stop()
       {  
	   if (thread != null)
	       {  
		   thread.stop();  
		   thread = null;
	       }
	   try
	       {  
		   if (console   != null)  console.close();
		   if (streamOut != null)  streamOut.close();
		   if (socket    != null)  socket.close();
	       }
	   catch(IOException ioe)
	       {  
		   System.out.println("Error closing ..."); 
	       }
	   client.close();  
	   client.stop();
       }

    /**********************************************
     *  METHOD:  
     *  PRECONDITIONS:  
     *  POSTCONDITIONS:  
     *********************************************/    

       public void run()
       {  
	   while (thread != null)
	       {  
		   try
		       {  
			   String plaintext = console.readLine();
			   plaintext = username + ":  " + plaintext;
			   String cipherOut = acm.encrypt(plaintext, secretKey);
			   streamOut.writeUTF(cipherOut);
			   streamOut.flush();
		       }

		   catch (Exception e)
		       {
			   System.out.println("Exception:  " +  e.getMessage());
			   e.printStackTrace();
			   stop();
		       }
	       }
       }

	public void sendSecretKey()
	{

	    try{
		String encodedSecretKey = 
		    acm.encryptSecretKeyWithPublicKeyAsString 
		    (secretKey, serverPublicKey);
		streamOut.writeUTF(encodedSecretKey);
	    }
	    catch(Exception e)
		{
		    e.printStackTrace();
		}
	}

	public void receivePublicKey(String cipher)
	{
	    try 
		{
		    serverPublicKey = acm.convertStringToPublicKey(cipher);
		    receivedPublicKey=true;
		    sendSecretKey();
		    secretKeySent=true;
		    thread.sleep(500);
		    sendPassword();
		}
	    catch (Exception e)
		{
		    e.printStackTrace();
		}
	}

	public void sendPassword()
	{
	    try 
		{
		    String hashedPassword = acm.createHashDigest(password);
		    String loginInfo = "6N0m3" + username +
			"9@55" + hashedPassword;
		    
		    String cipherOut = acm.encrypt(loginInfo, secretKey);
		    streamOut.writeUTF(cipherOut);
		    streamOut.flush();
		}
	    catch (Exception e)
		{
		    e.printStackTrace();
		}
	}
    /**********************************************
     *  METHOD:  
     *  PRECONDITIONS:  
     *  POSTCONDITIONS:  
     *********************************************/    
       public void handle(String cipher)
	{  
	    String msg="";
	    if (!receivedPublicKey || !secretKeySent)
		{
		    receivePublicKey(cipher);
		}
	    else
		{
		    try 
			{
			    msg = acm.decrypt(cipher, secretKey);
			}
		    
		    catch (Exception e)
			{
			    System.out.println("Exception:  " +  e.getMessage());
			    e.printStackTrace();
			    stop();
			}
		    
		    if (msg.equals(".bye"))
			{  
			    System.out.println("Good bye. Press RETURN to exit ...");
			    stop();
			}
		    else
			{
			    System.out.println(msg);
			}
		}
	}

    public static void main(String args[])
    {  
	ChatClientFinal client = null;
	if (args.length != 4)
	    {
		System.out.println("Usage: java ChatClientFinal username password host port");
	    }
	else
	    {
		client = new ChatClientFinal (args[0], Integer.parseInt(args[1]), args[2], args[3]);
	    }
    }

    }

